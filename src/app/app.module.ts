import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { ClarityModule } from 'clarity-angular';
/* App Root */
import { AppComponent }   from './app.component';

/* Feature Modules */
import { DashboardModule }    from './dashboard/dashboard.module';
import { CoreModule }       from './core/core.module';

/* Routing Module */
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  imports: [
    BrowserModule,
    ClarityModule.forRoot(),
    DashboardModule,
/*
    CoreModule,
*/
    CoreModule.forRoot({userName: 'Miss Marple'}),
    AppRoutingModule
  ],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
