import { Component, OnInit } from '@angular/core';
import { ActivatedRoute }    from '@angular/router';

import { Student,
         StudentService }    from './student.service';

@Component({
  templateUrl: './student-detail.component.html'
})
export class StudentDetailComponent implements OnInit {
  student: Student;

  constructor(
    private route: ActivatedRoute,
    private studentService: StudentService) { }

  ngOnInit() {
    let id = parseInt(this.route.snapshot.params['id'], 10);
    this.studentService.getStudent(id).then(student => this.student = student);
  }
}
