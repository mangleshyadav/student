import { NgModule }            from '@angular/core';

import { SharedModule }        from '../shared/shared.module';

import { StudentComponent }       from './student.component';
import { StudentDetailComponent } from './student-detail.component';
import { StudentListComponent }   from './student-list.component';
import { StudentRoutingModule }   from './student-routing.module';

@NgModule({
  imports: [ SharedModule, StudentRoutingModule ],
  declarations: [
    StudentComponent, StudentDetailComponent, StudentListComponent,
  ]
})
export class StudentModule { }
