// Exact copy except import UserService from core
import { Component, OnInit }      from '@angular/core';
import { UserService }             from '../core/user.service';

@Component({

  selector: 'app-dashboard',
  
  templateUrl: './dashboard.component.html',
  // styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {


  msg = 'Loading dashboards ...';
  userName = '';

  constructor( userService: UserService) {
    this.userName = userService.userName;
  }

  ngOnInit() {
    
  }


}
